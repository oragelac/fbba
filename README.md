# Modèle du site internet de la fédération belge des banques alimentaires #

Ce dépôt contient le modèle du site de la FBBA,  
dans le cadre d'un projet visant à revoir l'identité graphique  
de la fédération par des étudiants de l'IHECS.

J'ai contribué à ce projet en implémentant le HTML/CSS afin que  
le modèle du site soit le plus proche possible de la vision des étudiants de l'IHECS.

Ce projet s'est donc avéré assez ardu et il m'a fallu être rigoureux dans la partie design du site.  
Celui-ci m'aura permis d'en apprendre énormément sur le langage CSS et m'aura permis de mettre en pratique  
mes connaissances dans le domaine du développement web.


## Comment visualiser ce site internet ? ##

Il suffit de télécharger le contenu de ce dépôt et d'ouvrir le fichier nommé "index.html"  
à l'aide d'un navigateur internet pour arriver sur la page d'accueil du site web.

![fbba.png](https://bitbucket.org/repo/z88aRxy/images/992451556-fbba.png)