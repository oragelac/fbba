$(function(){  
	      setInterval(function(){  
	         $(".slideshow ul").animate({marginLeft:-275},800,function(){  
	            $(this).css({marginLeft:0}).find("li:last").after($(this).find("li:first"));  
	         })  
	      }, 2500);  
	   });

function toggle()
{
	jQuery('.toggle').hide();
	jQuery('a.toggler').click(function()
	{
		jQuery(this).next().toggle();
		var src = (jQuery("img", this).attr("src") === './images/arrows/down.png') ? './images/arrows/up.png' : './images/arrows/down.png';	
		var txt = (jQuery(".text", this).text() === "Replier ") ? "Lire la suite " : "Replier ";
		jQuery("img", this).attr("src", src);
		jQuery(".text", this).text(txt);

		return false;
	});
}

function animateBackgroundColor(selector, color1, color2)
{
	jQuery(selector).hover(function(){jQuery(this).animate({backgroundColor: color1},'fast');},function(){jQuery(this).animate({backgroundColor: color2},'fast');});
}

function modifyImageOnHover(selector, image1, image2)
{
	jQuery(selector).mouseover(function(){jQuery(this).attr('src', image1);});
	jQuery(selector).mouseout(function(){jQuery(this).attr('src', image2);});
}

function activateColorbox(selector)
{
	jQuery(selector).colorbox({iframe:true, innerWidth:"60%", innerHeight:"85%", fixed:true, opacity:0.5});
}

modifyImageOnHover("#devenezBenevole", "./images/devenezBenevoleHover.png", "./images/devenezBenevole.png");
modifyImageOnHover("#faitesDon", "./images/faitesDonHover.png", "./images/faitesDon.png");
modifyImageOnHover("#devenezPartenaire", "./images/devenezPartenaireHover.png", "./images/devenezPartenaire.png");
modifyImageOnHover(".okButton", "./images/okButtonHover.png", "./images/okButton.png");
modifyImageOnHover("#logo img", "./images/logoHover.png", "./images/logo.png");
modifyImageOnHover("#newsletter", "./images/newsletterButtonHover.png", "./images/newsletterButton.png");

activateColorbox("#donateForm");
activateColorbox("#volunteerForm");
activateColorbox("#partnerForm");
activateColorbox(".okButton");






